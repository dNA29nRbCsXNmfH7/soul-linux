from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys
from os import *


class Window(QMainWindow):
	def __init__(self):
		super().__init__()
		self.setWindowTitle("Radiator OS Greeter")
		self.setGeometry(100, 100, 600, 400)
		self.UiComponents()
		self.show()

	def UiComponents(self):
		button = QPushButton("Launch Installer", self)
		button.setGeometry(100, 50, 150, 50)
		button.clicked.connect(self.click)

	def click(self):
		print("Launching installer")
        #system("pkexec calamares")


App = QApplication(sys.argv)
window = Window()

# execute window
sys.exit(App.exec())
